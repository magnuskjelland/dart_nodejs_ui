console.log("Web Server Started go to 'http://localhost:3000' in your Browser.");
const path = require('path');
const basePath = __dirname;
var http = require('http');
var fs = require('fs');
// var index = fs.readFileSync( 'index.html');
var serial = require('./serial_interface');

// var assets = path.join(basePath,'assets');

const { SerialPort } = require('serialport')
const { ReadlineParser } = require('@serialport/parser-readline')
const { DelimiterParser } = require('@serialport/parser-delimiter')
const port = new SerialPort({ path: '/dev/ttyACM0', baudRate: 115200
})
const parser = port.pipe(new DelimiterParser({ delimiter: '\n' }))

var app = http.createServer(function(req, res) {
    if(req.url === "/"){
        fs.readFile("./public/index.html", "UTF-8", function(err, html){
            res.writeHead(200, {"Content-Type": "text/html"});
            res.end(html);
        });
    }else if(req.url.match("\.css$")){
        var cssPath = path.join(__dirname, 'public', req.url);
        var fileStream = fs.createReadStream(cssPath, "UTF-8");
        res.writeHead(200, {"Content-Type": "text/css"});
        fileStream.pipe(res);

    }else if(req.url.match("\.png$")){
        var imagePath = path.join(__dirname, 'public', req.url);
        var fileStream = fs.createReadStream(imagePath);
        res.writeHead(200, {"Content-Type": "image/png"});
        fileStream.pipe(res);
    }else{
        res.writeHead(404, {"Content-Type": "text/html"});
        res.end("No Page Found");
    }
});

var io = require('socket.io')(app);

io.on('connection', function(socket) {

    console.log('Node is listening to port');
    socket.on('led', function (data) {

        brightness = data.value;


        // console.log();
        // console.log(buf)
        let buf = serial.encode(data)
        port.write(buf)

        // io.sockets.emit('led', {value: brightness});
    });
});


parser.on('data', function(data) {
    let [output_data, new_data] = serial.decode(data)
    if (new_data)
        io.emit('chart', output_data);
});


app.listen(3000);